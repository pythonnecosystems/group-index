# Python과 개발 환경

이 group은 Python 프로그래밍 언어와 프로그래밍 개발 환경에 관한 읽을 만한 것들을 모았다.

이 포스트는 주로 그룹에 관한 인데스와 앞으로 올리거나 작업 중인 포스트들을 정리한 것이다.

## 차례
- [FastAPI](https://gitlab.com/pythonnecosystems/fastapi)
- [LearnFastAPI](https://gitlab.com/pythonnecosystems/learnfastapi)
- [Testing](https://gitlab.com/pythonnecosystems/testing)
- [Exception and Error Handling](https://pythonnecosystems.gitlab.io/exception-error-handling/)
- [Python Backend Framework](https://pythonnecosystems.gitlab.io/pythonbackendframework/)
- [Python Multiprocessing and Multithreading](https://pythonnecosystems.gitlab.io/pythonmultiprocessingnmultithreading/)
- [Starlette](https://pythonnecosystems.gitlab.io/starlette/)
- [Python Best Practices: 좋은 크드 작성을 위하여 꼭 필요한 10 방법](https://pythonnecosystems.gitlab.io/tech2write-cleaner-python-code/)
- [Awesome Terminal Applications](https://pythonnecosystems.gitlab.io/awesome-terminal-applications/)
- [Why did we choose FAST API over Flask and Django for our RESTFUL Micro-services](https://pythonnecosystems.gitlab.io/why-fastapi-for-restful-microservices/)
- <sup><img src="./images.png" width="15" height="15" /></sup> [Streamlit vs. Taipy — The Ultimate Comparison](https://pythonnecosystems.gitlab.io/streamlit-vs-taipy/)
- <sup><img src="./images.png" width="15" height="15" /></sup> [Structuring Python Project](https://pythonnecosystems.gitlab.io/structuring-python-project/)
- <sup><img src="./images.png" width="15" height="15" /></sup> [Enabling GoogleAuth for Fast API](https://pythonnecosystems.gitlab.io/fastapi/googleauth4fastapi/)
- <sup><img src="./images.png" width="15" height="15" /></sup> [8 Python Features You Should Know](https://pythonnecosystems.gitlab.io/8-python-important-features)
- <sup><img src="./images.png" width="15" height="15" /></sup> [Building a Web Application with Starlette and Uvicorn: A Beginner’s Tutorial](https://pythonnecosystems.gitlab.io/building-web-application-with-starlette-and-uvicorn/)
- <sup><img src="./images.png" width="15" height="15" /></sup> [FastAPI CRUD app with Raw SQL](https://pythonnecosystems.gitlab.io/fastapi/fastapi-crud-app-with-raw-sql/)
- [환상적인 파이썬 빌드 도구 5가지 🧑‍💻](https://python.plainenglish.io/python-build-tools-8aa38a158c82)

## 작업중
- [Pydantic](https://pythonnecosystems.gitlab.io/pydantic/)
- [Pydantic: 10 Overlooked Features You Should Be Using](https://medium.com/@kasperjuunge/pydantic-10-overlooked-features-you-should-be-using-8f0bac05c60c)
- [Twelve-Factor Python applications using Pydantic Settings](https://medium.com/datamindedbe/twelve-factor-python-applications-using-pydantic-settings-f74a69906f2f)
- [SQLAlchemy — A Short Introduction For Beginners](https://medium.com/gitconnected/introduction-to-sqlalchemy-bbf5af69a458)
- [SQLAlchemy vs. Raw SQL Queries in Python: A Comparative Example](https://python.plainenglish.io/sqlalchemy-vs-raw-sql-queries-in-python-a-comparative-example-b7a838ebef82)
- [FastAPI crud with MySQL](https://medium.com/@miladev95/fastapi-crud-with-mysql-b06d33601a38)
- [Fantastic 5 Python Build Tools Every Python Developer Should Have 🧑‍💻](https://python.plainenglish.io/python-build-tools-8aa38a158c82)
- [FastAPI : Testing a Database](https://medium.com/@jeanb.rocher/setting-up-the-project-f24ccbec5f9f)
- [Building a TODO API with FastAPI and SQLAlchemy: A Step-by-Step Guide](https://ktechhub.medium.com/building-a-todo-api-with-fastapi-and-sqlalchemy-a-step-by-step-guide-dbf5fe2fc7cd)
- [PyTest Tutorial: What is, How to Install, Framework, Assertions](https://www.guru99.com/pytest-tutorial.html)
- [13 Interesting Python Scripts](https://medium.com/nerd-for-tech/13-interesting-python-scripts-ffa0d2c1095d)
- [Writing an API using FastAPI: setting up the code — Hacking With Python](https://towardsdev.com/writing-an-api-using-fastapi-setting-up-the-code-hacking-with-python-f6633e9e9473)


## 작업 목록
- [AGSI](https://asgi.readthedocs.io/en/latest/)
- [Artificial Intelligence for Beginners - A Curriculum](https://microsoft.github.io/AI-For-Beginners/?fbclid=iwar0rm9aivyim2b2p24558b2pspa0vqjib-xngfygoe9nuq69epyp_gzt1gk&id=artificial-intelligence-for-beginners-a-curriculum)
- [Build Large Data Pipelines Using Taipy](https://towardsdev.com/build-large-data-pipelines-using-taipy-22922714751b)
- [Building a File Upload and Download API with Python and FastAPI](https://medium.com/@chodvadiyasaurabh/building-a-file-upload-and-download-api-with-python-and-fastapi-3de94e4d1a35)
- [Building Dashboards and Data Apps with Streamlit](https://medium.com/dp6-us-blog/building-dashboards-and-data-apps-with-streamlit-e1217cd3f00a)
- [Beyond FastAPI: The Evolution of Python Microservices in 2024 with PyNest](https://medium.com/@itay2803/stop-using-raw-fastapi-this-is-how-microservices-created-with-python-in-2024-a3ffbf57d103)
- [Create your first RestAPI using FastAPI](https://medium.com/featurepreneur/create-your-first-restapi-using-fastapi-fc0249877bef)
- [Elaborate Microservice async example with FastAPI, RabbitMQ, MongoDB and Redis — part1.](https://medium.com/@wilde.consult/elaborate-microservice-async-example-with-fastapi-rabbitmq-mongodb-and-redis-part1-4e5516cc8d4d)
- [FastAPI Microservice Patterns: Local Development Environment](https://python.plainenglish.io/fastapi-microservice-patterns-local-development-environment-12182e786f1c)
- [FastAPI-Celery-Flower-Docker async example — part1](https://medium.com/@wilde.consult/fastapi-celery-flower-docker-async-example-part1-8e1c6b631d51)
- [Flask API Folder Guide 2023](https://medium.com/@ashleyalexjacob/flask-api-folder-guide-2023-6fd56fe38c00)
- [How to Install PostgreSQL on a Mac using Docker](https://scriptable.com/how-to-install-postgresql-mac-docker/)
- [Making FastAPI pydantic schemas faster](https://sayanc20002.medium.com/making-fastapi-pydantic-schemas-faster-2ffdb1ce85a7)
- [Python — Logs with the logging Package](https://lukianovihor.medium.com/python-logs-with-the-logging-package-0ea3a033607f)
- [pytest fixtures are magic!](https://medium.com/@fwiles/pytest-fixtures-are-magic-fe25549e3b85)
- [Sharing State in FastAPI](https://rathoreaparna678.medium.com/sharing-state-in-fastapi-7f86034e8656)
- [Taipy GUI: It is specifically used to build GUI applications.](https://medium.com/p/1706d6974a37/)
- [Taipy Core: It provides an advanced backend for data-driven applications tailored to business use cases.](https://medium.com/p/22922714751b)
- [Simple React Application with FastAPI + Typescript + Postgres](https://medium.com/dev-genius/simple-react-application-with-fastapi-typescript-postgres-3c19acda47fa)


